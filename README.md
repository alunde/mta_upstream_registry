# README #

Clone this repo and build the web module for detailed reasoning and instructions.

Note: You'll need to first need to remove the requires section of the web module in the mta.yaml file so that the web module won't fail to build.  Put the section back once you've decided on your strategy and have the local and remote NodeJS modules running.

# Short Answer #
```
xs login -u XSA_ADMIN	
	
xs target -s SAP

xs set-env di-local-npm-registry UPSTREAM_LINK http://registry.npmjs.org/

xs restage di-local-npm-registry

xs restart di-local-npm-registry

xs env di-local-npm-registry | grep UPSTREAM_LINK
```

Verify with this, first discover the url for your local registry.
```
xs app di-local-npm-registry --urls
```
https://<di-local-npm-registry_host:port>

In a browser window
```
https://<di-local-npm-registry_host:port>/get-age
```
You should see the JSON result like this.

{
	name: "get-age",
	versions: {
		1.0.0: {...},
		1.0.1: {...}
	},
	dist-tags: {},
	_rev: "2-347a4280230f5231",
	readme: "# get-age ... ",
	_attachments: { }
}

Now attempt to build and run the remote module again.